MAC Changer is a python program used to change your MAC address

Usage

$sudo python mac_changer.py -i <interface> -m <new MAC address>

-i --interface  (set the interface; eth0, wlan0)
-m --mac    (set the new MAC address)
--help  Help